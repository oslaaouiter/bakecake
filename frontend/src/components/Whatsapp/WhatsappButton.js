import React from 'react';
import './whatsappButton.css';

export default function WhatsappButton(props) {
    const userInfo = props.userInfo;
    const order = props.order;
    const { shippingAddress, orderItems } = order;

    const buildMessage = () => {
        const items = orderItems.reduce(
            (accumulator, item) => accumulator + "\n \t\t▪" + item.name + " * " + item.quantity
            , "\n \t Items :"
        );
        return "New order : 📦🥳"
            + "\n \t Shipping Addresse : " + shippingAddress.address + "," + shippingAddress.city + "," + shippingAddress.postalCode
            + "\n \t Total Price :" + order.totalPrice + "MAD"
            + items
            + "\n\t nom : " + userInfo.name
            + "\n\t payment method : " + order.paymentMethod;
    }
    const redirectHandler = () => {
        window.open(`https://api.whatsapp.com/send?phone=212663100227&text=${encodeURI(buildMessage())}`);
    }
    return (
        <div
            onClick={redirectHandler}
            align="center" className="row whatsappbutton">
            <i className="fa fa-whatsapp fa-2x"></i>
            <div>
                <span>Order now</span><br />
                <span>with Whatsapp</span>
            </div>
        </div>
    )
}
