import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { detailsOrder } from '../actions/orderActions';
import LoadingBox from '../components/LoadingBox/LoadingBox';
import MessageBox from '../components/MessageBox/MessageBox';
import WhatsappButton from '../components/Whatsapp/WhatsappButton';

export default function OrderScreen(props) {
    const userSignin = useSelector(state => state.userSignin)
    const { userInfo } = userSignin;
    if (!userInfo) {
        props.history.push('/signin')
    }
    const orderId = props.match.params.id;
    const orderDetails = useSelector((state) => state.orderDetails);
    const { order, loading, error } = orderDetails;
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(detailsOrder(orderId));
    }, [dispatch, orderId]);
    return loading ? (
        <LoadingBox></LoadingBox>
    ) : error ? (
        <MessageBox variant="danger">{error}</MessageBox>
    ) : (
        <div>
            <h1>Order {order._id}</h1>
            <div className="row top">
                <div className="col-2">
                    <ul>
                        <li>
                            <div className="card card-body">
                                <h2>Shipping</h2>
                                <div className="row">
                                    <div className="col-1">
                                        <strong>Name:</strong> {order.shippingAddress.fullName}<br />
                                    </div>
                                    <div className="col-2">
                                        <strong>Address:{' '}</strong>
                                        {order.shippingAddress.address},
                                        {order.shippingAddress.city},
                                        {order.shippingAddress.postalCode}
                                    </div>
                                </div>
                                {order.isDelivered ? (
                                    <MessageBox variant="success">Delivered at {order.deliveredAt}</MessageBox>
                                ) : (
                                    <MessageBox variant="danger">Not Delivered</MessageBox>
                                )}
                            </div>
                        </li>
                        <li>
                            <div className="card card-body">
                                <h2>Payment</h2>
                                <p>
                                    <strong>Method:</strong> {order.paymentMethod}<br />
                                </p>
                                {order.isPaid ? (
                                    <MessageBox variant="success">Paid at {order.paidAt}</MessageBox>
                                ) : (
                                    <MessageBox variant="danger">Not paid</MessageBox>
                                )}
                            </div>
                        </li>
                        <li>
                            <div className="card card-body">
                                <h2>Order Items</h2>
                                <ul>
                                    {
                                        order.orderItems.map((item) => (
                                            <li key={item.product}>
                                                <div className="row">
                                                    <div>
                                                        <img
                                                            src={item.image}
                                                            alt={item.name}
                                                            className="small"
                                                        ></img>
                                                    </div>
                                                    <div className="min-30">
                                                        <Link to={`/product/${item.product}`}>{item.name}</Link>
                                                    </div>
                                                    <div>
                                                        {item.quantity} * {item.price}= {item.quantity * item.price} MAD
                                                    </div>
                                                </div>
                                            </li>
                                        ))
                                    }
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
                <div className="col-1">
                    <div className="card card-body">
                        <ul>
                            <li>
                                <h2>Order Summary</h2>
                            </li>
                            <li>
                                <div className="row">
                                    <div>Items</div>
                                    <div>{order.itemsPrice} MAD</div>
                                </div>
                            </li>
                            <li>
                                <div className="row">
                                    <div>Shipping</div>
                                    <div>{order.shippingPrice} MAD</div>
                                </div>
                            </li>
                            <li>
                                <div className="row">
                                    <div>Tax</div>
                                    <div>{order.taxPrice} MAD</div>
                                </div>
                            </li>
                            <li>
                                <div className="row">
                                    <div><strong>Order Total</strong></div>
                                    <div><strong>{order.totalPrice} MAD</strong></div>
                                </div>
                            </li>
                            <li>
                                {
                                    !order.isPaid && order.paymentMethod === "cashOnDelivery" && (
                                        <WhatsappButton userInfo={userInfo} order={order} />
                                    )
                                }
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}
