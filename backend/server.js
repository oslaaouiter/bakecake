import express from 'express';
import mongoose from 'mongoose'
import productRouter from './routers/prodcutRouter.js';
import userRouter from './routers/userRouter.js';
import dotenv from 'dotenv';
import orderRouter from './routers/orderRouter.js';


dotenv.config();//dotenv for .env file that contains all secret password for this application
const app = express();

//To translate http data to req.body to be used
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//Monggose to connect to database and manage documents
mongoose.connect(process.env.MONGODB_URL || 'mongodb://localhost/bakecake', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
})

//Routers
app.use('/api/users', userRouter);
app.use('/api/products', productRouter);
app.use('/api/orders', orderRouter);


app.get('/', (req, res) => {
    res.send('Server is ready');
});

app.use((err, req, res, next) => {
    res.status(500).send({ message: err.message });
})//for Error handling


const port = process.env.PORT || 5000;
app.listen(port, () => {
    console.log(`Server at http://localhost:${port}`);
})