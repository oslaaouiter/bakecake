import bcrypt from 'bcryptjs';
const data = {
    users: [
        {
            name: 'Zeinab',
            email: 'zeinab.souihi@gmail.com',
            phoneNumber: '0616828995',
            password: bcrypt.hashSync('123456', 8),
            isAdmin: true,
        },
        {
            name: 'Oualid',
            email: 'oualid.slaaouiter@gmail.com',
            phoneNumber: '0616828995',
            password: bcrypt.hashSync('123456', 8),
            isAdmin: false,
        },
    ],
    products: [
        {
            name: 'Cinnamon rolls',
            category: 'sugary',
            image: '/images/product-1.jpg',
            price: 120,
            rating: 4,
            numReviews: 7,
            description: 'High quality Cinnamon rolls , really tasty',
            countInStock: 5
        },
        {
            name: 'Donuts',
            category: 'sugary',
            image: '/images/product-2.jpg',
            price: 120,
            rating: 4.5,
            numReviews: 4,
            description: 'You can\'t buy happiness but you can buy donuts',
            countInStock: 0
        },
        {
            name: 'Cookies',
            category: 'sugary',
            image: '/images/product-3.jpg',
            price: 300,
            rating: 4.5,
            numReviews: 3,
            description: 'Oreo Cookies, Caramel Cookies & Peanut butter ',
            countInStock: 5
        },
        {
            name: 'Tiramisu Desset Cups',
            category: 'sugary',
            image: '/images/product-4.jpg',
            price: 200,
            rating: 3,
            numReviews: 7,
            description: 'High quality Tiramisu Desset Cups , really tasty',
            countInStock: 5
        },
        {
            name: 'Beignets',
            category: 'sugary',
            image: '/images/product-5.jpg',
            price: 120,
            rating: 3,
            numReviews: 6,
            description: 'Beignets à la crème pâtissière à la vanille 😍 Vanilla custard cream filled doughnuts 🤤',
            countInStock: 5
        },
        {
            name: 'Maccaron',
            category: 'sugary',
            image: '/images/product-6.jpg',
            price: 120,
            rating: 4.5,
            numReviews: 9,
            description: 'High quality Maccaron , really tasty',
            countInStock: 5
        }
    ]
}
export default data;